# "Vpdate"

**VpdateCLI** is a wrapper that allows one click CLI updats for Void Linux. You can use it as a backend for a desktop shortcut, a panel icon, or a menu entry in your app launcher.\
When launching **VpdateCLI** you are presented with the following choices:
- clear cache after updating
- remove orphans after updating
- show updated services to consider a system restart
- update Flatpaks

### Dependencies
- bash
- xtools (for the restart option)
- flatpak (optional)

### How to use

**VpdateCLI** and **VpdateCLI_Starter** need to be executable and in your \$PATH.\ If you like to use it as a one click updater, just create a launcher for it. As an example I included a launcher script that calls for KDE Konsol, **VpdateCLI_Starter**. Just make a new menu entry or a shortcut or any kind of button or icon and refer to this or you own launcher script.

Download:

`git clone https://gitlab.com/Irkiosan/voidlinux-update-script.git`

Make the scripts executable:

`chmod +x ./voidlinux-update-script/{VpdateCLI,VpdateCLI_Starter}`

Move the script to a folder in your $PATH (e.g. /usr/bin):

`sudo mv ./voidlinux-update-script/VpdateCLI /usr/bin`

Move the starter script anywhere (e.g. /home/\<USER\>/.config)

`mv ./voidlinux-update-script/VpdateCLI_Starter /home/<USER>/.config`

Create a desktop icon, a menu entry, a panel applet... or whatever you want to use and add the path to your starter script to it (e.g. /home/\<USER\>/.config/VpdateCLI_Starter).

Remove the downloaded files:

`rm -R ./voidlinux-update-script/`

### What the Vpdate does

- checks if xbps is up to date and updates it when necessary
- syncs repositories
- starts the regular update
- clears cache if chosen
- removes the orphans if chosen
- shows updated services to consider a system restart if chosen
- updates flatpak if chosen


